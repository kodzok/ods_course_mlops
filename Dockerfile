FROM mambaorg/micromamba

ARG YOUR_ENV=prod

ENV POETRY_VERSION=1.8.2
ENV CONDA_VENV=mlops_ods
ENV POETRY_PATH=/root/.local/bin

WORKDIR /app
COPY environment.yml ./

USER root
RUN micromamba create -f environment.yml -y
RUN micromamba run -n $CONDA_VENV pipx install poetry==${POETRY_VERSION}

ENV PATH="${POETRY_PATH}:${PATH}"

COPY pyproject.toml poetry.lock ./
RUN if [ "$YOUR_ENV" = "prod" ]; \
    then \
        micromamba run -n $CONDA_VENV poetry install --only main --no-root; \
    else \
        micromamba run -n $CONDA_VENV poetry install --no-root; \
    fi
