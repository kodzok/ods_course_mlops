"""
This module provides functionality for generating and displaying random numbers.
"""

import numpy as np


def generate_random_number() -> int:
	"""
	Generate a random integer between 1 and 100.

	Returns:
		int: A random integer between 1 and 100.
	"""
	return int(np.random.randint(low=1, high=101))


def main() -> None:
	"""
	Generate a random number and print it to the console.
	"""
	number = generate_random_number()
	print(f"Your random number is: {number}")


if __name__ == "__main__":
	main()
