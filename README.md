# ML Project
This is project for ODS MLOps Course.
## Usage
*[Instructions how to use or run project]*

## Building and Running Docker Container
To run the application in a Docker container, follow these simple instructions:
1. **Build Docker Image**

   In the root directory of the project, execute the command to build the Docker image:
   - with **production** environment only:
       ```bash
       docker build -t <IMAGE_NAME> .
       ```
   - with **dev** environment:
        ```bash
     docker build --build-arg YOUR_ENV=dev -t <IMAGE_NAME> .
     ```
2. **Run Docker Container**

    After successfully building the image, you can run the Docker container:
    ```bash
   docker run --rm <IMAGE_NAME>
    ```

## Repository Management Methodology
We follow a structured approach to managing our repository.
Here are some key practices we adhere to:

- **Branching Model**: We use the GitHub flow branching model for our
development workflow.
  - The `master` branch represents the latest
  stable version of the project. It should always be in a deployable
  state.
  - When working on a new feature or enhancement, create a new
  branch off of `master`. Name the branch descriptively
  (e.g., `feature/new-feature-name`) and make commits to this
  branch.
  - Once your feature is complete, open a merge request to merge
  your changes into `master`. Merge requests should include a
  summary of changes and any related issues or tasks.
  - All merge requests require at least one approval from another
  team member before they can be merged. Reviewers should provide
  feedback and ensure code quality before approving the changes.
  - Once a merge request is approved and any conflicts are
  resolved, it can be merged into `master`. Make sure to delete
  the feature branch after merging.


- **Issue Tracking**: We use GitLab Issues to track bugs, feature
requests, and other tasks. When contributing, please check the
existing issues or create a new one to discuss your proposed
changes.
