# Contributing to ML project

## How to contribute
1. Fork the repository.

2. Clone your forked repository locally:
    ```bash
   git clone https://gitlab.com/your-username/ods_course_mlops.git
    ```

3. Set up the project:
   ```bash
   cd ods_course_mlops
   conda env create -f environment.yml -y
   conda run -n mlops_ods poetry install --no-root
   conda run -n mlops_ods pre-commit install
   ```
4. Create a new branch:
   ```bash
   git checkout -b feature/your-feature-name
   ```
5. Make your changes and commit them:
   ```bash
   git add .
   git commit -m "Add your commit message here"
   ```
6. Push to your branch:
   ```bash
   git push origin feature/your-feature-name
   ```
7. Open a merge request on GitLab's repository page.

## Code Quality and Linters
We use [pre-commit](https://pre-commit.com/) hooks to maintain code quality, including
linting with [Ruff](https://docs.astral.sh/ruff/) and type checking with [Mypy](https://github.com/pre-commit/mirrors-mypy). These hooks run
checks before allowing you to commit your changes. Make sure to
address any issues raised by these checks before committing your
code.

To manually run pre-commit checks, use:
   ```bash
   conda run -n mlops_ods pre-commit run --all-files
   ```
